# Code Quality

This example shows how to run Code Quality on your code by using GitLab CI/CD and Docker.
First, you need GitLab Runner with [docker-in-docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor).
Once you set up the Runner, add a new job to .gitlab-ci.yml that generates the expected report

The above example will create a code_quality job in your CI/CD pipeline which will scan your source code for code quality issues. The report will be saved as a Code Quality report artifact that you can later download and analyze. Due to implementation limitations we always take the latest Code Quality artifact available.